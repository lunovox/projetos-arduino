#include "pitches.h"

// notes in the melody:
int pinSom = 9;
int pinLed = 6; //13

int melody[] = {
  NOTE_C4, NOTE_G3,NOTE_G3, NOTE_A3, NOTE_G3,0, NOTE_B3, NOTE_C4};

// note durations: 4 = quarter note, 8 = eighth note, etc.:
int noteDurations[] = {
  4, 8, 8, 4,4,4,4,4 };

void setup() {
  // iterate over the notes of the melody:
  pinMode(pinLed, OUTPUT);
  pinMode(pinSom, OUTPUT);
  Serial.begin(9600); 
  
}

void loop() {
  // no need to repeat the melody.
  doPlaySound();
  delay(3000);
}

void doPlaySound(){
  for (int thisNote = 0; thisNote < 8; thisNote++) {

    // to calculate the note duration, take one second 
    // divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    digitalWrite(pinLed, HIGH);
    int noteDuration = 1000/noteDurations[thisNote];
    tone(pinSom, melody[thisNote],noteDuration);
    delay(noteDuration);
    digitalWrite(pinLed, LOW);
    

    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    // stop the tone playing:
    noTone(pinSom);
  }
  Serial.println("------------------------------------");
}

int convertNotaToLed(int valNota){
  //return map(valNota, NOTE_B0, NOTE_DS8, 0, 255);
  int valLed = map(valNota, 100, 300, 0, 255);
  Serial.print("Nota = " );                       
  Serial.print(valNota);      
  Serial.print("\t Led = ");    
  Serial.println(valLed);
  return valLed;
}
