
// FONTE: http://ferpinheiro.wordpress.com/2011/08/28/tocando-musicas-com-o-arduino/

int pinSom = 9;
int pinLed = 13;
//speaker connected to one of the PWM ports
 
#define c 261
#define d 294
#define e 329
#define f 349
#define g 391
#define gS 415
#define a 440
#define aS 455
#define b 466
#define cH 523
#define cSH 554
#define dH 587
#define dSH 622
#define eH 659
#define fH 698
#define fSH 740
#define gH 784
#define gSH 830
#define aH 880
//frequencies for the tones we're going to use
//used http://home.mit.bme.hu/~bako/tonecalc/tonecalc.htm to get these
 
void setup()
{
  pinMode(pinLed, OUTPUT);
  // sets the pinLed to be an output
  pinMode(pinSom, OUTPUT);
  //sets the pinSom to be an output
}
 
void loop()     // run over and over again
{
  march();
}
 
void beep (unsigned char pinSom, int frequencyInHertz, long timeInMilliseconds)
{
    digitalWrite(pinLed, HIGH);
    //use led to visualize the notes being played
 
    int x;
    long delayAmount = (long)(1000000/frequencyInHertz);
    long loopTime = (long)((timeInMilliseconds*1000)/(delayAmount*2));
    for (x=0;x<=loopTime;x++){
        digitalWrite(pinSom,HIGH);
        delayMicroseconds(delayAmount);
        digitalWrite(pinSom,LOW);
        delayMicroseconds(delayAmount);
    }
 
    digitalWrite(pinLed, LOW);
    //set led back to low
 
    delay(20);
    //a little delay to make all notes sound separate
}
 
void march()
{
    //for the sheet music see:
    //http://www.musicnotes.com/sheetmusic/mtd.asp?ppn=MN0016254
    //this is just a translation of said sheet music to frequencies / time in ms
    //used 500 ms for a quart note
 
    beep(pinSom, a, 500);
    beep(pinSom, a, 500);
    beep(pinSom, a, 500);
    beep(pinSom, f, 350);
    beep(pinSom, cH, 150);
 
    beep(pinSom, a, 500);
    beep(pinSom, f, 350);
    beep(pinSom, cH, 150);
    beep(pinSom, a, 1000);
    //first bit
 
    beep(pinSom, eH, 500);
    beep(pinSom, eH, 500);
    beep(pinSom, eH, 500);
    beep(pinSom, fH, 350);
    beep(pinSom, cH, 150);
 
    beep(pinSom, gS, 500);
    beep(pinSom, f, 350);
    beep(pinSom, cH, 150);
    beep(pinSom, a, 1000);
    //second bit...
 
    beep(pinSom, aH, 500);
    beep(pinSom, a, 350);
    beep(pinSom, a, 150);
    beep(pinSom, aH, 500);
    beep(pinSom, gSH, 250);
    beep(pinSom, gH, 250);
 
    beep(pinSom, fSH, 125);
    beep(pinSom, fH, 125);
    beep(pinSom, fSH, 250);
    delay(250);
    beep(pinSom, aS, 250);
    beep(pinSom, dSH, 500);
    beep(pinSom, dH, 250);
    beep(pinSom, cSH, 250);
    //start of the interesting bit
 
    beep(pinSom, cH, 125);
    beep(pinSom, b, 125);
    beep(pinSom, cH, 250);
    delay(250);
    beep(pinSom, f, 125);
    beep(pinSom, gS, 500);
    beep(pinSom, f, 375);
    beep(pinSom, a, 125);
 
    beep(pinSom, cH, 500);
    beep(pinSom, a, 375);
    beep(pinSom, cH, 125);
    beep(pinSom, eH, 1000);
    //more interesting stuff (this doesn't quite get it right somehow)
 
    beep(pinSom, aH, 500);
    beep(pinSom, a, 350);
    beep(pinSom, a, 150);
    beep(pinSom, aH, 500);
    beep(pinSom, gSH, 250);
    beep(pinSom, gH, 250);
 
    beep(pinSom, fSH, 125);
    beep(pinSom, fH, 125);
    beep(pinSom, fSH, 250);
    delay(250);
    beep(pinSom, aS, 250);
    beep(pinSom, dSH, 500);
    beep(pinSom, dH, 250);
    beep(pinSom, cSH, 250);
    //repeat... repeat
 
    beep(pinSom, cH, 125);
    beep(pinSom, b, 125);
    beep(pinSom, cH, 250);
    delay(250);
    beep(pinSom, f, 250);
    beep(pinSom, gS, 500);
    beep(pinSom, f, 375);
    beep(pinSom, cH, 125);
 
    beep(pinSom, a, 500);
    beep(pinSom, f, 375);
    beep(pinSom, c, 125);
    beep(pinSom, a, 1000);
    //and we're done \ó/
}
