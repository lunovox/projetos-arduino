#include "pitches.h"

int pinSom = 9;
int pinLed = 6;

//Sweet Child O Mine- Guns N Roses-------------------------------------------------------------------------------------------------------------------------------
int mainRiffD[] = {NOTE_D4 , NOTE_D5 , NOTE_A4, NOTE_G4, NOTE_G5, NOTE_A4, NOTE_FS5, NOTE_A4};
int mainRiffE[] = {NOTE_E4 , NOTE_D5 , NOTE_A4, NOTE_G4, NOTE_G5, NOTE_A4, NOTE_FS5, NOTE_A4};
int mainRiffG[] = {NOTE_G4 , NOTE_D5 , NOTE_A4, NOTE_G4, NOTE_G5, NOTE_A4, NOTE_FS5, NOTE_A4};
    
int mainRiffDurations[] = {
//d4  d5  a4   g4  g5  g4  fs5  a4  
  6,  6,  6,   6,  6,  6,  6 ,  6};
//----------------------------------------------------------------------------------------------------------------------------------------------
void setup() {
  Serial.begin(9600);

}
    
void loop() {
  delay(3000);
  doPlaySound();
}

void doPlaySound(){
  for(int introTwoTimes = 0; introTwoTimes < 2; introTwoTimes++){
    for(int dTwice = 0; dTwice < 2; dTwice++){
      for (int thisNote = 0; thisNote < 8; thisNote++){
        int mainRiffDuration = 1000/mainRiffDurations[thisNote];
        tone(pinSom, mainRiffD[thisNote], mainRiffDuration);
        analogWrite(pinLed, convertNotaToLed(mainRiffD[thisNote]));
        int pauseBetweenNotes = mainRiffDuration * 1.30;
        delay(pauseBetweenNotes);
        noTone(pinSom);
      }
    }
    
    for(int eTwice = 0; eTwice < 2; eTwice++){
      for (int thisNote = 0; thisNote < 8; thisNote++){
        int mainRiffDuration = 1000/mainRiffDurations[thisNote];
        tone(pinSom, mainRiffE[thisNote], mainRiffDuration);
        analogWrite(pinLed, convertNotaToLed(mainRiffE[thisNote]));
        int pauseBetweenNotes = mainRiffDuration * 1.30;
        delay(pauseBetweenNotes);
        noTone(pinSom);
      }
    }
    
    for(int gTwice = 0; gTwice < 2; gTwice++){
      for (int thisNote = 0; thisNote < 8; thisNote++){
        int mainRiffDuration = 1000/mainRiffDurations[thisNote];
        tone(pinSom, mainRiffG[thisNote], mainRiffDuration);
        analogWrite(pinLed, convertNotaToLed(mainRiffG[thisNote]));
        int pauseBetweenNotes = mainRiffDuration * 1.30;
        delay(pauseBetweenNotes);
        noTone(pinSom);
      }
    }
    
    for(int dTwice = 0; dTwice < 2; dTwice++){
      for (int thisNote = 0; thisNote < 8; thisNote++){
        int mainRiffDuration = 1000/mainRiffDurations[thisNote];
        tone(pinSom, mainRiffD[thisNote], mainRiffDuration);
        analogWrite(pinLed, convertNotaToLed(mainRiffD[thisNote]));
        int pauseBetweenNotes = mainRiffDuration * 1.30;
        delay(pauseBetweenNotes);
        noTone(pinSom);
      }
    }
  }
}

int convertNotaToLed(int valNota){
  //return map(valNota, NOTE_B0, NOTE_DS8, 0, 255);
  int valLed = map(valNota, 200, 800, 0, 255);
  Serial.print("Som = " );                       
  Serial.print(valNota);      
  Serial.print("\t Led = ");    
  //Serial.println(map(valLed, 0, 255, 0, 100) + " percent");
  Serial.println(valLed);
  return valLed;
}

