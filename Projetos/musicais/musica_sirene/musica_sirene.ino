int PinSom = 9;    // LED connected to digital pin 9
int PinLed = 6;
int valMin = 512;
int valMax = 1024;
int valDelay = 5; //20
int valPasso = 15;


void setup()  { 
  // nothing happens in setup 
  pinMode(PinLed, OUTPUT);
  pinMode(PinSom, OUTPUT);
} 

void loop()  { 
  // fade in from min to max in increments of 5 points:
  for(int valExit = valMin ; valExit<=valMax; valExit +=valPasso) { 
    doShow(valExit);                            
  } 

  // fade out from max to min in increments of 5 points:
  for(int valExit = valMax ; valExit>=valMin; valExit -=valPasso) { 
    doShow(valExit);
  } /**/
}

void doShow(int valExit){
  analogWrite(PinLed, map(valExit, valMin, valMax, 0, 255));         
  tone(PinSom,valExit);
  // wait for 30 milliseconds to see the dimming effect    
  delay(valDelay);
}


