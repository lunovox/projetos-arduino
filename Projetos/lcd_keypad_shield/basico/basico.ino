// Programa : Teste LCD 16x2 com Keypad
// Autor : Arduino e Cia
  
#include <LiquidCrystal.h>  
  
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);  //Registe Select:8; Enable:9; Dados:4+5+6+7;
  
void setup(){  
    lcd.begin(16, 2);  
    lcd.clear();
    //lcd.scrollDisplayLeft(); //Rola o Display 1 vez para esquerda
    //lcd.cursor(); //Cursor Visivel
    lcd.noCursor();
    //lcd.blink(); //Cursor Piscando
    lcd.noBlink();
    
    lcd.setCursor(0,0);  
    lcd.print("LCD Keypad"); //LCD Keypad Shield
    lcd.setCursor(0,1);  
    lcd.print("Tecla: ");  
    
}  
  
void loop(){  
	int botao = analogRead(0);  //Leitura do valor a porta analógica A0

	//lcd.setCursor(11,0);  
        //lcd.print (millis());  

	lcd.setCursor(7,1);  
	String btnText = "val(";          // string which will contain the result
	btnText = btnText + botao + ")         ";
	//btnText = btnText + millis();
	lcd.print (btnText);
        
	if (botao < 100) {  
		//lcd.print ("Direita ");  
		//lcd.scrollDisplayRight(); //Rola o Display 1 vez para direita
		printLetra(3, 11,0); //Imprime a letra 3 na coluna 11 linha 0
	}else if(botao < 200){  
		//lcd.print ("Cima  ");  
		printLetra(1, 11,0); //Imprime a letra 1 na coluna 11 linha 0
	}else if(botao < 400){  
		//lcd.print ("Baixo  ");  
		printLetra(2, 11,0); //Imprime a letra 2 na coluna 11 linha 0
	}else if(botao < 600){  
		//lcd.print ("Esquerda");  
		//lcd.scrollDisplayLeft(); //Rola o Display 1 vez para esquerda
		printLetra(4, 11,0); //Imprime a letra 4 na coluna 11 linha 0
	}else if(botao < 800){  
		//lcd.print ("Select ");  
	}else if(botao >= 800){  
		lcd.print ("--------");  
      printLetra(0, 11,0); //Imprime a letra 0 na coluna 11 linha 0
	}  
	//delay(250);
}

void printLetra(int letra, int col, int line){  
  byte stickman[5][8] = {
    { //0 = normal
      B01110,
      B01110,
      B00100,
      B01110,
      B10101,
      B00100,
      B01010,
      B01010
    },{//1 = braco levantado
      B01110,
      B01110,
      B10101,
      B01110,
      B00100,
      B00100,
      B01010,
      B10001
    },{//2 = corpo abaixado
      B00000,
      B01110,
      B01110,
      B00100,
      B01110,
      B10101,
      B01110,
      B01010
    },{//3 = corpo direita
      B00110,
      B00110,
      B00100,
      B00111,
      B00100,
      B00110,
      B00101,
      B01101
    },{//4 = corpo esquerda
      B01100,
      B01100,
      B00100,
      B11100,
      B00100,
      B01100,
      B10100,
      B10110
    }
  };
 
  lcd.createChar(0,stickman[0]); //mar o caracter 0 como o stickman normal
  lcd.createChar(1,stickman[1]); //mar o caracter 1 como o stickman de bracos levantado
  lcd.createChar(2,stickman[2]); //mar o caracter 2 como o stickman abaixado
  lcd.createChar(3,stickman[3]); //mar o caracter 3 como o stickman de corpo para direita
  lcd.createChar(4,stickman[4]); //mar o caracter 3 como o stickman de corpo para esquerda
  lcd.setCursor(col,line);  
  lcd.write(byte(letra)); //Imprime o caracter letr como no local marcado.
  //delay(1000);
  /**/
}
